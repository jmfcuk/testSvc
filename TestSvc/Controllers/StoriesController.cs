﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApp.Controllers.Api
{
	public class TocItem
	{
		public int Id { get; set; }
		public string Desc { get; set; }
	}

	public class TocList : List<TocItem> { }

	[EnableCors(origins: "*", headers: "*", methods: "GET, POST")]
	public class StoriesController : ApiController
	{
		[HttpGet]
		public TocList Contents()
		{
			TocList cl = new TocList();

			for (int x = 0; x < 10; x++)
			{
				cl.Add(new TocItem()
				{
					Id = x + 1,
					Desc = $"Toc {x + 1}",
				});
			}

			return cl;
		}

		[HttpGet]
		public object Story(int id)
		{
			return new
			{
				Id = id,
				Text = "Test",
			};
		}
	}
}


